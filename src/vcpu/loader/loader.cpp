#include "../memory/segmenttable.hpp"

#include "loader.hpp"

void load(ExecOptions *execOpts)
{
    execOpts->memorySize = alignValue(execOpts->memorySize, sizeof(size_t));
    execOpts->memory = malloc(execOpts->memorySize);

    execOpts->codeSize = deserialize(execOpts->executablePath, execOpts);

    execOpts->codeSegmentSize = alignValue(execOpts->codeSize, sizeof(size_t));
    execOpts->rstackSize = alignValue(execOpts->rstackSize, sizeof(size_t));
    execOpts->rstackStart = (size_t)execOpts->memory + execOpts->codeSegmentSize;
    execOpts->stackSize = alignValue(execOpts->stackSize, sizeof(size_t));
    execOpts->stackStart = execOpts->rstackStart + execOpts->rstackSize;

    execOpts->memorySizeRequired =
        execOpts->codeSegmentSize +
        execOpts->rstackSize +
        execOpts->stackSize +
        sizeof(Segment) * 2;
}
