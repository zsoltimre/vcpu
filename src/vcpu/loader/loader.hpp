#ifndef __LOADER_HEADER__
#define __LOADER_HEADER__

#include <stdlib.h>

#include "../../libserializer/serializer.hpp"

#include "../utils/utils.hpp"

struct ExecOptions
{
    char magic[4];
    char *executablePath = NULL;
    void *memory = NULL;
    size_t memorySize = 0;
    size_t memorySizeRequired = 0;
    bool debug = false;

    size_t codeSize = 0;
    size_t codeSegmentSize = 0;

    size_t rstackSize = 0;
    size_t rstackStart = 0;

    size_t stackSize = 0;
    size_t stackStart = 0;

    size_t heapSize = 0;
    void *heapStart = NULL;
};

void load(ExecOptions *execOpts);

#endif
