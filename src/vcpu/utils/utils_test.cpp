#include <gtest/gtest.h>

#include "utils.hpp"

#include <vector>
#include <iostream>
#include <string.h>

TEST(alignValue, AlignValue)
{
    EXPECT_EQ(alignValue(1, 8), 8);
    EXPECT_EQ(alignValue(7, 8), 8);
    EXPECT_EQ(alignValue(8, 8), 8);
    EXPECT_EQ(alignValue(1, 16), 16);
    EXPECT_EQ(alignValue(18, 16), 32);
}
