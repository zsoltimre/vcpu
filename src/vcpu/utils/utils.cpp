#include "utils.hpp"

size_t alignValue(size_t value, size_t size)
{
    return ((value + size - 1) / size) * size;
}
