#ifndef __LIB_CPU_INSTRUCTIONS_HEADER__
#define __LIB_CPU_INSTRUCTIONS_HEADER__

#include <stdexcept>

#include "../cpu.hpp"
#include "../memory/segmenttable.hpp"
#include "../registers/registers.hpp"

enum Instructions : size_t
{
    HALT = 1,
    SEH,
    CALL,
    RET,
    JMP,
    JLT,
    JGT,
    JEQ,
    JF,
    PUSH,
    PUSHR,
    POPR,
    STR,
    LDR,
    MOV,
    MOVR,
    CMP,
    CMPR,
    ADD,
    ADDR,
    SUB,
    SUBR,
    MUL,
    MULR,
    DIV,
    DIVR,
    PUTC,
    PRINT,
};

typedef void (*Handler)(
    SegmentTable *segmentTable,
    size_t codeStart,
    Register *Reg);

Handler getInstructionHandler(size_t opcode);

void prevent_pr_write(size_t registerNumber);

void op_halt(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_seh(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_push(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_pushr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_popr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_jump(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_call(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_ret(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_str(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_ldr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_mov(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_movr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_add(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_addr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_sub(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_subr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_mul(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_mulr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_div(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_divr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_cmp(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_cmpr(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_jeq(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_jlt(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_jgt(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_jf(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_putc(SegmentTable *segmentTable, size_t codeStart, Register *Reg);
void op_print(SegmentTable *segmentTable, size_t codeStart, Register *Reg);

#endif
