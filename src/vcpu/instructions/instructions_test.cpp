#include <gtest/gtest.h>

#include "../../common/faults.hpp"
#include "../registers/registers.hpp"

#include "instructions.hpp"

TEST(prevent_pr_write, PreventModifyReturnPointer) {
    prevent_pr_write(R0);
    EXPECT_ANY_THROW({
        prevent_pr_write(RP);
    });
    EXPECT_ANY_THROW({
        prevent_pr_write(EH);
    });
}

struct Context {
    void *memory = NULL;
    SegmentTable segmentTable;
};

void setupTestEnvironment(
    Context *context,
    size_t memorySize,
    size_t rstackSize,
    size_t stackSize,
    char *code,
    size_t codeSize,
    Register *Reg
) {
    context->memory = malloc(memorySize);
    memcpy(context->memory, (void *)code, codeSize);
    Reg[IP].value = (size_t)context->memory;
    context->segmentTable = SegmentTable((void *)(Reg[IP].value + codeSize));
    Reg[RP].value = Reg[IP].value + codeSize + sizeof(Segment) * 2;
    Reg[SP].value = Reg[RP].value + rstackSize;
    context->segmentTable.registerSegment((char *)"rstack", Reg[RP].value, rstackSize, SEGF_STATIC | SEGF_RO);
    context->segmentTable.registerSegment((char *)"stack", Reg[SP].value, stackSize, SEGF_STATIC | SEGF_RW);
    // Set initial positions outside of the memory region. This way we can
    // have a clean push/pop behaviour and implementation, and utilise the
    // entire allocated memory.
    Reg[RP].value -= sizeof(size_t);
    Reg[SP].value -= sizeof(size_t);
}

TEST(op_seh, SetErrorHandler) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    op_seh(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t));
    EXPECT_EQ(Reg[EH].value, (size_t)context->memory + (5 * sizeof(size_t)));

    free(context->memory);
    free(context);
}

TEST(op_push, PushNumericValueToStack) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    op_push(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xfd);

    Reg[IP].value = (size_t)context->memory;
    memcpy(context->memory, (char *)"\xfe\x00\x00\x00\x00\x00\x00\x00", sizeof(code));
    op_push(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xfe);

    Reg[IP].value = (size_t)context->memory;
    memcpy(context->memory, (char *)"\xff\x00\x00\x00\x00\x00\x00\x00", sizeof(code));
    op_push(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_STACK_OOBOP);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xfe);

    free(context->memory);
    free(context);
}

TEST(op_pushr, PushRegisterValueToStack) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { R10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[R10].value = 0xffff;
    op_pushr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xffff);

    Reg[IP].value = (size_t)context->memory;
    Reg[R10].value = 0xfffe;
    op_pushr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xfffe);

    Reg[IP].value = (size_t)context->memory;
    Reg[R10].value = 0xfffd;
    op_pushr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_STACK_OOBOP);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xfffe);

    free(context->memory);
    free(context);
}

TEST(op_popr, PopValueFromStackToRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    op_push(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xffff);

    Reg[IP].value = (size_t)context->memory;
    uint8_t code_1[] = { R0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    memcpy(context->memory, (char *)code_1, sizeof(code));
    op_popr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R0].value, 0xffff);

    Reg[IP].value = (size_t)context->memory;
    memcpy(context->memory, (char *)code_1, sizeof(code));
    op_popr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_STACK_OOBOP);
    EXPECT_EQ(Reg[R0].value, 0xffff);

    free(context->memory);
    free(context);
}

TEST(op_popr, PopValueFromStackToProtectedRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    op_push(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[SP].value, 0xffff);

    Reg[IP].value = (size_t)context->memory;
    uint8_t code_1[] = { RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    memcpy(context->memory, (char *)code_1, sizeof(code));
    EXPECT_ANY_THROW({
        op_popr(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_jump, JumpToAddress) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    op_jump(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + 24);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 5);

    free(context->memory);
    free(context);
}

TEST(op_call, CallFunction) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 64, 64, (char *)code, sizeof(code), Reg);
    Reg[EH].value = 0;

    Reg[IP].value += sizeof(size_t);
    op_call(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, 0);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 5);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 3);

    free(context->memory);
    free(context);
}

TEST(op_call, CallFunctionRstackOob) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 8, (char *)code, sizeof(code), Reg);
    Reg[EH].value = 0;

    op_call(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 2);

    op_call(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 2);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 3);

    op_call(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_RSTACK_OOBOP);
    // IP should still point to the next instruction
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 3);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 4);

    free(context->memory);
    free(context);
}

TEST(op_ret, ReturnToCaller) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 8, (char *)code, sizeof(code), Reg);
    Reg[EH].value = 0;

    op_call(&context->segmentTable, (size_t)context->memory, Reg);
    op_ret(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);

    op_ret(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_RSTACK_OOBOP);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);

    free(context->memory);
    free(context);
}

TEST(op_str, StoreRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Segment seg;
    context->segmentTable.getSegmentEntry((char *)"stack", &seg);
    size_t targetAddress = seg.end;
    // Let's write after the stack segment, which is allocated, but
    // unused memory area.

    Reg[R0].value = 0xffffffffffffffff;
    Reg[R1].value = targetAddress;
    op_str(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(*(size_t *)targetAddress, 0xffffffffffffffff);

    free(context->memory);
    free(context);
}

TEST(op_str, StoreRegisterValueInReadOnlySegment) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Segment seg;
    context->segmentTable.getSegmentEntry((char *)"rstack", &seg);
    size_t targetAddress = seg.start;

    Reg[R0].value = 0xffffffffffffffff;
    Reg[R1].value = targetAddress;
    op_str(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_SEG_FORBIDDEN_WRITE);
    EXPECT_NE(*(size_t *)targetAddress, 0xffffffffffffffff);

    free(context->memory);
    free(context);
}

TEST(op_ldr, LoadValueFromDataToRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Segment seg;
    context->segmentTable.getSegmentEntry((char *)"stack", &seg);
    size_t targetAddress = seg.end;

    Reg[R0].value = 0xfefefefefefefefe;
    Reg[R1].value = targetAddress;
    op_str(&context->segmentTable, (size_t)context->memory, Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[R0].value = 0x0;
    Reg[R1].value = targetAddress;
    op_ldr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R0].value, 0xfefefefefefefefe);

    free(context->memory);
    free(context);
}

TEST(op_ldr, LoadValueFromDataToProtectedRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 256, 16, 16, (char *)code, sizeof(code), Reg);

    Segment seg;
    context->segmentTable.getSegmentEntry((char *)"stack", &seg);
    size_t targetAddress = seg.start;

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = targetAddress;
    EXPECT_ANY_THROW({
        op_ldr(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_mov, SetRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    op_mov(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R2].value, 5);

    free(context->memory);
    free(context);
}

TEST(op_mov, SetProtectedRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    EXPECT_ANY_THROW({
        op_mov(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_movr, SetRegisterValueFromRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R2].value = 0x0;
    Reg[R10].value = 0xff;
    op_movr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R2].value, 0xff);

    free(context->memory);
    free(context);
}

TEST(op_movr, SetProtectedRegisterValueFromRegister) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R2].value = 0x0;
    Reg[R10].value = 0xff;
    EXPECT_ANY_THROW({
        op_movr(&context->segmentTable, (size_t)context->memory, Reg);
    });
    EXPECT_EQ(Reg[R2].value, 0x0);

    free(context->memory);
    free(context);
}

TEST(op_add, AddValueToRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 20;
    op_add(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 29);

    free(context->memory);
    free(context);
}

TEST(op_add, AddValueToProtectedRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    EXPECT_ANY_THROW({
        op_add(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_addr, AddRegisterValueToRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 20;
    Reg[R2].value = 53;
    op_addr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 73);

    free(context->memory);
    free(context);
}

TEST(op_addr, AddRegisterValueToProtectedRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R2].value = 53;
    EXPECT_ANY_THROW({
        op_addr(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_sub, SubtractValueFromRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 15;
    op_sub(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 5);

    free(context->memory);
    free(context);
}

TEST(op_sub, SubtractValueFromProtectedRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 15;
    EXPECT_ANY_THROW({
        op_sub(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_subr, SubtractRegisterValueFromRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 15;
    Reg[R2].value = 5;
    op_subr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 10);

    free(context->memory);
    free(context);
}

TEST(op_subr, SubtractRegisterValueFromProtectedRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R2].value = 5;
    EXPECT_ANY_THROW({
        op_subr(&context->segmentTable, (size_t)context->memory, Reg);
    });
    free(context->memory);
    free(context);
}

TEST(op_mul, MultiplyRegisterValueByValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 5;
    op_mul(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 25);

    free(context->memory);
    free(context);
}

TEST(op_mul, MultiplyProtectedRegisterValueByValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    EXPECT_ANY_THROW({
    op_mul(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_mulr, MultiplyRegisterValueByRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 5;
    Reg[R2].value = 5;
    op_mulr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 25);

    free(context->memory);
    free(context);
}

TEST(op_mulr, MultiplyProtectedRegisterValueByRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 5;
    EXPECT_ANY_THROW({
        op_mulr(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_div, DivideRegisterValueByValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 25;
    op_div(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 5);

    // Including these test cases here just to signal that the below are 
    // (for now) the expected behaviour from `div*` operations.
    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 6;
    op_div(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 1);
    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 4;
    op_div(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 0);

    free(context->memory);
    free(context);
}

TEST(op_div, DivideRegisterValueByZero) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 25;
    op_div(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_DIVZERO);
    EXPECT_EQ(Reg[R1].value, 25);

    free(context->memory);
    free(context);
};

TEST(op_div, DivideProtectedRegisterValueByValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    EXPECT_ANY_THROW({
        op_div(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_divr, DivideRegisterValueByRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R1].value = 25;
    Reg[R2].value = 5;
    op_divr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 5);

    // Division by zero
    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 25;
    Reg[R2].value = 0;
    op_divr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FR].value, FAULT_DIVZERO);
    EXPECT_EQ(Reg[R1].value, 25);
    EXPECT_EQ(Reg[R2].value, 0);

    // Including these test cases here just to signal that the below are 
    // (for now) the expected behaviour from `div*` operations.
    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 5;
    Reg[R2].value = 4;
    op_divr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 1);

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 4;
    Reg[R2].value = 5;
    op_divr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[R1].value, 0);

    free(context->memory);
    free(context);
}

TEST(op_divr, DivideProtectedRegisterValueByRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        RP, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[R0].value = 2;
    EXPECT_ANY_THROW({
        op_divr(&context->segmentTable, (size_t)context->memory, Reg);
    });

    free(context->memory);
    free(context);
}

TEST(op_cmp, CompareRegisterValueToValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 25;
    op_cmp(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 0);

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 30;
    op_cmp(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 2);


    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 20;
    op_cmp(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 1);

    free(context->memory);
    free(context);
}

TEST(op_cmpr, CompareRegisterValueToRegisterValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        R1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        R2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 25;
    Reg[R2].value = 25;
    op_cmpr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 0);

    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 30;
    Reg[R2].value = 25;
    op_cmpr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 2);


    Reg[IP].value = (size_t)context->memory;
    Reg[R1].value = 20;
    Reg[R2].value = 25;
    op_cmpr(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[FLR].value, 1);

    free(context->memory);
    free(context);
}

TEST(op_jeq, JumpOnlyIfValuesEqual) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 0;
    op_jeq(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 3);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 1);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 1;
    op_jeq(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 2;
    op_jeq(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    free(context->memory);
    free(context);
}

TEST(op_jlt, JumpOnlyIfValueLessThanValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 1;
    op_jlt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 3);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 1);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 0;
    op_jlt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 2;
    op_jlt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    free(context->memory);
    free(context);
}

TEST(op_jgt, JumpOnlyIfValueLessThanValue) {
    Register *Reg = getRegisters();
    Context *context = (Context *)malloc(sizeof(Context));
    uint8_t code[] = { 
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    setupTestEnvironment(context, 128, 16, 16, (char *)code, sizeof(code), Reg);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 2;
    op_jgt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 3);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 1);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 1;
    op_jgt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    Reg[IP].value = (size_t)context->memory;
    Reg[FLR].value = 0;
    op_jgt(&context->segmentTable, (size_t)context->memory, Reg);
    EXPECT_EQ(Reg[IP].value, (size_t)context->memory + sizeof(size_t) * 1);
    EXPECT_EQ(*(size_t *)Reg[IP].value, 0);

    free(context->memory);
    free(context);
}
