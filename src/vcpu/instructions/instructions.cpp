#include <stdio.h>

#include "../../common/faults.hpp"
#include "../registers/registers.hpp"
#include "../memory/segmenttable.hpp"

#include "instructions.hpp"

size_t InstructionHandlers[] = {
    (size_t)0,
    (size_t)op_halt,
    (size_t)op_seh,
    (size_t)op_call,
    (size_t)op_ret,
    (size_t)op_jump,
    (size_t)op_jlt,
    (size_t)op_jgt,
    (size_t)op_jeq,
    (size_t)op_jf,
    (size_t)op_push,
    (size_t)op_pushr,
    (size_t)op_popr,
    (size_t)op_str,
    (size_t)op_ldr,
    (size_t)op_mov,
    (size_t)op_movr,
    (size_t)op_cmp,
    (size_t)op_cmpr,
    (size_t)op_add,
    (size_t)op_addr,
    (size_t)op_sub,
    (size_t)op_subr,
    (size_t)op_mul,
    (size_t)op_mulr,
    (size_t)op_div,
    (size_t)op_divr,
    (size_t)op_putc,
    (size_t)op_print,
};

Handler getInstructionHandler(size_t opcode)
{
    if (opcode > sizeof(InstructionHandlers))
    {
        return NULL;
    }
    return (Handler)InstructionHandlers[opcode];
}

/**
 * Do not allow code to modify protected registers.
 */
void prevent_pr_write(size_t registerNumber)
{
    size_t no_write_regs[] = { IP, RP, EH };
    for (size_t pos = 0; pos < 3; pos++) {
        if (no_write_regs[pos] == registerNumber) {
            throw std::invalid_argument("Invalid instruction: writing protected register denied.");
        }
    }
}

void op_seh(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t errorHandlerMemOffset = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[EH].value = codeStart + errorHandlerMemOffset * sizeof(size_t);
}

void op_halt(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targetRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t returnCode = Reg[targetRegister].value;
    exit(returnCode);
}

void op_push(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (segmentTable->isOutOfBounds((char *)"stack", (size_t)Reg[SP].value + sizeof(size_t)))
    {
        Reg[FR].value = FAULT_STACK_OOBOP;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value + sizeof(size_t);
        return;
    }
    Reg[SP].value += sizeof(size_t);
    *(size_t *)Reg[SP].value = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
}

void op_pushr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (segmentTable->isOutOfBounds((char *)"stack", (size_t)Reg[SP].value + sizeof(size_t)))
    {
        Reg[FR].value = FAULT_STACK_OOBOP;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value + sizeof(size_t);
        return;
    }
    Reg[SP].value += sizeof(size_t);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    *(size_t *)Reg[SP].value = Reg[sourceRegister].value;
    Reg[IP].value += sizeof(size_t);
}

void op_popr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targetRegister = *(size_t *)Reg[IP].value;
    prevent_pr_write(targetRegister);
    Reg[IP].value += sizeof(size_t);
    if (segmentTable->isOutOfBounds((char *)"stack", (size_t)Reg[SP].value))
    {
        Reg[FR].value = FAULT_STACK_OOBOP;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value;
        return;
    }
    Reg[targetRegister].value = *(size_t *)Reg[SP].value;
    Reg[SP].value -= sizeof(size_t);
}

void op_jump(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t offset = *(size_t *)Reg[IP].value * sizeof(size_t);
    Reg[IP].value = codeStart + offset;
}

void op_call(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (segmentTable->isOutOfBounds((char *)"rstack", Reg[RP].value + (sizeof(size_t) * 2)))
    {
        Reg[FR].value = FAULT_RSTACK_OOBOP;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value + sizeof(size_t);
        return;
    }
    size_t value = codeStart + (*(size_t *)Reg[IP].value * sizeof(size_t));
    Reg[IP].value += sizeof(size_t);
    Reg[RP].value += sizeof(size_t) * 2;
    *(size_t *)Reg[RP].value = Reg[IP].value;
    *(size_t *)((size_t)Reg[RP].value + sizeof(size_t)) = 0; // TODO: RESERVED: Stack alloc ptr
    Reg[FR].value = 0;
    Reg[IP].value = value;
}

void op_ret(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (segmentTable->isOutOfBounds((char *)"rstack", (size_t)Reg[RP].value))
    {
        Reg[FR].value = FAULT_RSTACK_OOBOP;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value;
        return;
    }
    Reg[IP].value = *(size_t *)Reg[RP].value;
    Reg[RP].value -= sizeof(size_t) * 2;
}

void op_str(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t sourceReg = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t destinationReg = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t address = Reg[destinationReg].value;
    if (segmentTable->isReadOnlyByAddress((void *)address))
    {
        Reg[FR].value = FAULT_SEG_FORBIDDEN_WRITE;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value;
        return;
    }
    *(size_t *)address = Reg[sourceReg].value;
}

void op_ldr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t destinationReg = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t sourceReg = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(destinationReg);
    Reg[destinationReg].value = *(size_t *)Reg[sourceReg].value;
}

void op_mov(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targetRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targetRegister);
    size_t value = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[targetRegister].value = value;
}

void op_movr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targetRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targetRegister);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[targetRegister].value = Reg[sourceRegister].value;
}

void op_add(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    Reg[targerRegister].value += *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
}

void op_addr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[targerRegister].value += Reg[sourceRegister].value;
}

void op_sub(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    Reg[targerRegister].value -= *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
}

void op_subr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[targerRegister].value -= Reg[sourceRegister].value;
}

void op_mul(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    Reg[targerRegister].value = Reg[targerRegister].value * *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
}

void op_mulr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    Reg[targerRegister].value = Reg[targerRegister].value * Reg[sourceRegister].value;
}

void op_div(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    size_t value = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    if (value == 0)
    {
        Reg[FR].value = FAULT_DIVZERO;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value;
        return;
    }
    Reg[targerRegister].value = Reg[targerRegister].value / value;
}

void op_divr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t targerRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    prevent_pr_write(targerRegister);
    size_t sourceRegister = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t value = Reg[sourceRegister].value;
    if (value == 0)
    {
        Reg[FR].value = FAULT_DIVZERO;
        Reg[IP].value = Reg[EH].value > 0 ? Reg[EH].value : Reg[IP].value;
        return;
    }
    Reg[targerRegister].value = Reg[targerRegister].value / value;
}

void op_cmp(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t registerId = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t regValue = Reg[registerId].value;
    size_t value = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);

    if (regValue > value)
    {
        Reg[FLR].value = 2;
        return;
    }
    if (regValue == value)
    {
        Reg[FLR].value = 0;
        return;
    }
    Reg[FLR].value = 1;
}

void op_cmpr(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t registerIdA = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t regValue1 = Reg[registerIdA].value;

    size_t registerIdB = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t regValue2 = Reg[registerIdB].value;

    if (regValue1 > regValue2)
    {
        Reg[FLR].value = 2;
        return;
    }
    if (regValue1 == regValue2)
    {
        Reg[FLR].value = 0;
        return;
    }
    Reg[FLR].value = 1;
}

void op_jeq(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (Reg[FLR].value == 0)
    {
        Reg[IP].value = *(size_t *)Reg[IP].value * sizeof(size_t) + codeStart;
        return;
    }
    Reg[IP].value += sizeof(size_t);
}

void op_jlt(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (Reg[FLR].value == 1)
    {
        Reg[IP].value = *(size_t *)Reg[IP].value * sizeof(size_t) + codeStart;
        return;
    }
    Reg[IP].value += sizeof(size_t);
}

void op_jgt(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (Reg[FLR].value == 2)
    {
        Reg[IP].value = *(size_t *)Reg[IP].value * sizeof(size_t) + codeStart;
        return;
    }
    Reg[IP].value += sizeof(size_t);
}

void op_jf(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    if (Reg[FR].value != 0)
    {
        Reg[IP].value = *(size_t *)Reg[IP].value * sizeof(size_t) + codeStart;
        return;
    }
    Reg[IP].value += sizeof(size_t);
}

void op_putc(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t registerNumber = *(size_t *)Reg[IP].value;
    Reg[IP].value += sizeof(size_t);
    size_t address = Reg[registerNumber].value;
    char c = *(char *)address;
    putchar(c);
}

void op_print(SegmentTable *segmentTable, size_t codeStart, Register *Reg)
{
    size_t value = *(size_t *)Reg[SP].value;
    printf("SP: 0x%zx, value: 0x%zx\n", Reg[SP].value, value);
    return;
}
