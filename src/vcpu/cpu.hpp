#ifndef __CPU_HEADER__
#define __CPU_HEADER__

#include <stdio.h>
#include <stdlib.h>

#include "../vcpu/registers/registers.hpp"
#include "../vcpu/instructions/instructions.hpp"
#include "../vcpu/memory/segmenttable.hpp"
#include "loader/loader.hpp"

void execute(ExecOptions *execOpts);

#endif
