#include "cpu.hpp"

void execute(ExecOptions *execOpts)
{
    Register *Reg = getRegisters();

    size_t line = 0;
    Reg[IP].value = (size_t)execOpts->memory;
    // Set initial positions outside of the memory region. This way we can
    // have a clean push/pop behaviour and implementation, and utilise the
    // entire allocated memory.
    Reg[RP].value = (size_t)execOpts->rstackStart - sizeof(size_t);
    Reg[SP].value = (size_t)execOpts->stackStart - sizeof(size_t);
    SegmentTable segmentTable = SegmentTable((void *)(Reg[IP].value + execOpts->codeSize));
    segmentTable.registerSegment((char *)"rstack", (size_t)execOpts->rstackStart, execOpts->rstackSize, SEGF_STATIC | SEGF_RO);
    segmentTable.registerSegment((char *)"stack", (size_t)execOpts->stackStart, execOpts->stackSize, SEGF_STATIC | SEGF_RW);

    size_t codeEnd = (size_t)((size_t)execOpts->memory + execOpts->codeSize);
    while (Reg[IP].value < codeEnd)
    {
        size_t opcode = *(size_t *)Reg[IP].value;
        Reg[IP].value += sizeof(size_t);
        Handler handler = getInstructionHandler(opcode);
        if (handler == NULL)
        {
            fprintf(stderr, "Unsupported instruction at line %zu with opcode: %zu\n", line, opcode);
            exit(255);
        }
        handler(&segmentTable, (size_t)execOpts->memory, Reg);
        line++;
    }
}
