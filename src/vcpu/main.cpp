#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "../libserializer/serializer.hpp"

#include "loader/loader.hpp"
#include "utils/utils.hpp"
#include "cpu.hpp"

void usage(char *execName)
{
    fprintf(stdout, "Usage: %s [-h|-d] -m <memory_size> <path_to_executable>\n", execName);
}

void processOptions(ExecOptions *execOpts, int argc, char *argv[])
{
    int c;
    while ((c = getopt(argc, argv, "hdm:")) != -1)
    {
        switch (c)
        {
        case 'h':
            usage(argv[0]);
            exit(0);
        case 'd':
            execOpts->debug = true;
            break;
        case 'm':
            execOpts->memorySize = strtoul(optarg, NULL, 0);
            break;
        case '?':
            if (optopt == 'c')
            {
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                exit(1);
            }
            else if (isprint(optopt))
            {
                fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                exit(1);
            }
            else
            {
                fprintf(stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
                exit(1);
            }
        default:
            abort();
        }
    }
}

int main(int argc, char *argv[])
{
    ExecOptions execOpts;
    processOptions(&execOpts, argc, argv);

    if (execOpts.memorySize == 0)
    {
        fprintf(stderr, "Memory size was not set.\n");
        exit(1);
    }

    execOpts.executablePath = argv[optind];

    if (execOpts.executablePath == NULL)
    {
        fprintf(stderr, "Executable to run was not provided.\n");
        exit(1);
    }

    int retCode = 0;
    try
    {
        load(&execOpts);
        if (execOpts.memory == NULL)
        {
            throw std::invalid_argument("Failed to allocate memory for the CPU.");
        }
        if (execOpts.memorySize < execOpts.memorySizeRequired)
        {
            throw std::invalid_argument("Not enough memory.");
        }
        execute(&execOpts);
    }
    catch (char *msg)
    {
        fprintf(stderr, "Unexpected error: %s\n", msg);
        retCode = 2;
    }

    if (execOpts.memory != NULL)
    {
        free(execOpts.memory);
    }

    return retCode;
}
