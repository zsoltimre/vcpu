#include <gtest/gtest.h>

#include "registers.hpp"

#include <stdio.h>

TEST(getRegisters, UpdateRegisterValue) {
    Register *regs = getRegisters();
    regs[R0].value = 0x01;
    regs[R1].value = 0x02;

    size_t R0_addr = (size_t)regs + R0 * (sizeof(size_t) * 2);
    size_t R0_value_addr = R0_addr + sizeof(size_t);
    EXPECT_EQ(*(size_t *)R0_value_addr, 0x01);

    EXPECT_EQ(regs[EH].value, 0x00);

    size_t R1_addr = (size_t)regs + R1 * (sizeof(size_t) * 2);
    size_t R1_value_addr = R1_addr + sizeof(size_t);
    EXPECT_EQ(*(size_t *)R1_value_addr, 0x02);

    EXPECT_EQ(regs[EH].value, 0x00);

}
