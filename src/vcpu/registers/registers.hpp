#ifndef __REGISTERS_HEADER__
#define __REGISTERS_HEADER__

#include <stdlib.h>

struct Register {
    size_t id;
    size_t value;
};

enum Registers : size_t
{
    IP = 1, // Instruction pointer
    SP,     // Stack pointer
    BP,     // Base pointer
    RP,     // Return pointer. It works very similar to the stack pointer, but
            // it points to the return address stack (`rstack`).
            // Only the CPU should be able to modify the value of this register
            // when performing CALL* and RET instructions. From the perspective
            // of user code, it must act read-only.
    FR,     // Fault register to signal errors/exceptions.
    EH,     // Address of global error/exception handler function.
    FLR,
    // General purpose registers
    R0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
};

Register *getRegisters();

#endif
