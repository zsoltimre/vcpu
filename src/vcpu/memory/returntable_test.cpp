#include <gtest/gtest.h>

#include "segmenttable.hpp"
#include "returntable.hpp"

TEST(addEntry, AddNewReturnEntry)
{
    void *memory = malloc(512);
    SegmentTable segmentTable = SegmentTable(memory);
    size_t rstackAddr = (size_t)memory + sizeof(Segment) * 2;
    size_t stackAddr = rstackAddr + 128;
    segmentTable.registerSegment((char *)"rstack", (size_t)rstackAddr, 128, SEGF_STATIC | SEGF_RO);
    segmentTable.registerSegment((char *)"stack", (size_t)stackAddr, 256, SEGF_STATIC | SEGF_RW);

    ReturnTable returnTable = ReturnTable(&segmentTable, (void *)rstackAddr);

    EXPECT_EQ(returnTable.tableEntries, 0);
    bool result1 = returnTable.addEntry((void *)0xff00000000000001, (void *)0x01);
    EXPECT_EQ(result1, true);
    EXPECT_EQ(returnTable.tableEntries, 1);
    EXPECT_EQ(*(size_t *)rstackAddr, 0xff00000000000001);
    EXPECT_EQ(*(size_t *)(rstackAddr + sizeof(size_t)), 0x01);
    bool result2 = returnTable.addEntry((void *)0xff00000000000002, (void *)0x02);
    EXPECT_EQ(result2, true);
    EXPECT_EQ(returnTable.tableEntries, 2);
    EXPECT_EQ(*(size_t *)(rstackAddr + sizeof(ReturnEntry)), 0xff00000000000002);
    EXPECT_EQ(*(size_t *)(rstackAddr + sizeof(ReturnEntry) + sizeof(size_t)), 0x02);

    if (memory != NULL) {
        free(memory);
    }
}

TEST(addEntry, AddNewReturnEntryOob)
{
    void *memory = malloc(512);
    SegmentTable segmentTable = SegmentTable(memory);
    size_t rstackAddr = (size_t)memory + sizeof(Segment) * 2;
    size_t stackAddr = rstackAddr + 32;
    segmentTable.registerSegment((char *)"rstack", (size_t)rstackAddr, 32, SEGF_STATIC | SEGF_RO);
    segmentTable.registerSegment((char *)"stack", (size_t)stackAddr, 256, SEGF_STATIC | SEGF_RW);

    ReturnTable returnTable = ReturnTable(&segmentTable, (void *)rstackAddr);

    EXPECT_EQ(returnTable.tableEntries, 0);
    bool result1 = returnTable.addEntry((void *)0xff00000000000001, (void *)0x01);
    EXPECT_EQ(result1, true);
    EXPECT_EQ(returnTable.tableEntries, 1);
    bool result2 = returnTable.addEntry((void *)0xff00000000000002, (void *)0x02);
    EXPECT_EQ(result2, true);
    EXPECT_EQ(returnTable.tableEntries, 2);
    bool result3 = returnTable.addEntry((void *)0xff00000000000003, (void *)0x03);
    EXPECT_EQ(result3, false);
    EXPECT_EQ(returnTable.tableEntries, 2);

    if (memory != NULL) {
        free(memory);
    }
}

TEST(removeEntry, RemoveReturnEntry)
{
    void *memory = malloc(512);
    SegmentTable segmentTable = SegmentTable(memory);
    size_t rstackAddr = (size_t)memory + sizeof(Segment) * 2;
    size_t stackAddr = rstackAddr + 128;
    segmentTable.registerSegment((char *)"rstack", (size_t)rstackAddr, 128, SEGF_STATIC | SEGF_RO);
    segmentTable.registerSegment((char *)"stack", (size_t)stackAddr, 256, SEGF_STATIC | SEGF_RW);

    ReturnTable returnTable = ReturnTable(&segmentTable, (void *)rstackAddr);
    returnTable.addEntry((void *)0x01, (void *)0x02);
    returnTable.addEntry((void *)0x03, (void *)0x04);
    EXPECT_EQ(returnTable.tableEntries, 2);

    bool result = returnTable.removeEntry();
    EXPECT_EQ(result, true);
    EXPECT_EQ(returnTable.tableEntries, 1);

    EXPECT_EQ(*(size_t *)(rstackAddr + sizeof(ReturnEntry)), 0x0);
    EXPECT_EQ(*(size_t *)(rstackAddr + sizeof(ReturnEntry) + sizeof(size_t)), 0x0);

    if (memory != NULL) {
        free(memory);
    }
}

TEST(getLast, GetLastEntry)
{
    void *memory = malloc(512);
    SegmentTable segmentTable = SegmentTable(memory);
    size_t rstackAddr = (size_t)memory + sizeof(Segment) * 2;
    size_t stackAddr = rstackAddr + 128;
    segmentTable.registerSegment((char *)"rstack", (size_t)rstackAddr, 128, SEGF_STATIC | SEGF_RO);
    segmentTable.registerSegment((char *)"stack", (size_t)stackAddr, 256, SEGF_STATIC | SEGF_RW);

    ReturnTable returnTable = ReturnTable(&segmentTable, (void *)rstackAddr);
    returnTable.addEntry((void *)0x01, (void *)0x02);
    returnTable.addEntry((void *)0x03, (void *)0x04);
    EXPECT_EQ(returnTable.tableEntries, 2);

    ReturnEntry *lastEntry = returnTable.getLast();

    EXPECT_EQ((size_t)lastEntry->address, 0x03);
    EXPECT_EQ((size_t)lastEntry->stack, 0x04);

    if (memory != NULL) {
        free(memory);
    }
}
