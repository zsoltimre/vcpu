#include <gtest/gtest.h>

#include "segmenttable.hpp"

TEST(registerSegment, RegisterNewSegment)
{
    void *memory = malloc(128);
    SegmentTable table = SegmentTable(memory);

    void *rstackSegmentMemory = malloc(512);
    table.registerSegment((char *)"rstack", (size_t)rstackSegmentMemory, 512, SEGF_STATIC | SEGF_RO);
    EXPECT_EQ(table.tableEntries, 1);

    void *stackSegmentMemory = malloc(256);
    table.registerSegment((char *)"stack", (size_t)stackSegmentMemory, 256, SEGF_STATIC | SEGF_RW);
    EXPECT_EQ(table.tableEntries, 2);

    Segment rstackSegment;
    table.getSegmentEntry((char *)"rstack", &rstackSegment);
    EXPECT_STREQ(rstackSegment.name, "rstack");
    EXPECT_NE(rstackSegment.start, 0);
    EXPECT_EQ(rstackSegment.end, (size_t)rstackSegmentMemory + 512);
    EXPECT_EQ(rstackSegment.flags, 3);

    Segment stackSegment;
    table.getSegmentEntry((char *)"stack", &stackSegment);
    EXPECT_STREQ(stackSegment.name, "stack");
    EXPECT_NE(stackSegment.start, 0);
    EXPECT_EQ(stackSegment.end, (size_t)stackSegmentMemory + 256);
    EXPECT_EQ(stackSegment.flags, 2);

    free(rstackSegmentMemory);
    free(stackSegmentMemory);
    free(memory);
}

TEST(isReadOnlyBySegmentName, CheckIfSegmentIsReadOnly)
{
    void *memory = malloc(128);
    SegmentTable table = SegmentTable(memory);

    void *rstackSegmentMemory = malloc(32);
    table.registerSegment((char *)"rstack", (size_t)rstackSegmentMemory, 32, SEGF_STATIC | SEGF_RO);
    EXPECT_EQ(table.isReadOnlyBySegmentName((char *)"rstack"), true);

    void *stackSegmentMemory = malloc(32);
    table.registerSegment((char *)"stack", (size_t)stackSegmentMemory, 32, SEGF_STATIC | SEGF_RW);
    EXPECT_EQ(table.isReadOnlyBySegmentName((char *)"stack"), false);

    free(memory);
}


TEST(isReadOnlyByAddress, CheckIfMemoryAddressIsReadOnly)
{
    void *memory = malloc(128);
    SegmentTable table = SegmentTable(memory);

    void *rstackSegmentMemory = malloc(32);
    table.registerSegment((char *)"rstack", (size_t)rstackSegmentMemory, 32, SEGF_STATIC | SEGF_RO);
    Segment rstackSegment;
    table.getSegmentEntry((char *)"rstack", &rstackSegment);
    size_t rstackAddress = rstackSegment.start + 8;
    EXPECT_EQ(table.isReadOnlyByAddress((void *)rstackAddress), true);
    
    void *stackSegmentMemory = malloc(32);
    table.registerSegment((char *)"stack", (size_t)stackSegmentMemory, 32, SEGF_STATIC | SEGF_RW);
    Segment stackSegment;
    table.getSegmentEntry((char *)"stack", &stackSegment);
    size_t stackAddress = stackSegment.start + 8;
    EXPECT_EQ(table.isReadOnlyByAddress((void *)stackAddress), false);

    free(memory);
}
