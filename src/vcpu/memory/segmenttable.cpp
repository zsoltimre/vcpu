#include "segmenttable.hpp"

SegmentTable::SegmentTable(void *memory)
{
    this->tableAddress = memory;
    this->tableEntries = 0;
};

void SegmentTable::registerSegment(char *name, size_t start, size_t size, uint8_t flags)
{
    size_t storeAt = (size_t)this->tableAddress;
    if (this->tableEntries > 0)
    {
        storeAt += this->tableEntries * sizeof(Segment);
    }
    memset((void *)storeAt, 0, sizeof(Segment));

    Segment segmentData;
    segmentData.start = start;
    segmentData.end = start + size;
    segmentData.flags = flags;
    memcpy(segmentData.name, name, SEGMENT_NAME_MAX_LENGTH - 1);

    memcpy((void *)storeAt, &segmentData, sizeof(Segment));

    this->tableEntries++;
}

void SegmentTable::getSegmentEntry(char *name, void *segment)
{
    memset(segment, 0, sizeof(Segment));
    size_t address = (size_t)this->tableAddress;
    for (size_t pos = 0; pos < this->tableEntries; pos++)
    {
        Segment *storedSegment = (Segment *)address;
        if (strncmp(storedSegment->name, name, SEGMENT_NAME_MAX_LENGTH) == 0)
        {
            memcpy(segment, storedSegment, sizeof(Segment));
            return;
        }
        address += sizeof(Segment);
    }
}

bool SegmentTable::isOutOfBounds(char *name, size_t address)
{
    Segment seg;
    this->getSegmentEntry(name, &seg);
    return (address >= seg.end || address < seg.start);
}

bool SegmentTable::isReadOnlyBySegmentName(char *name)
{
    Segment seg;
    this->getSegmentEntry(name, &seg);
    return seg.flags & SEGF_RO;
}

bool SegmentTable::isReadOnlyByAddress(void *address)
{
    bool readOnly = false;
    size_t tableEntryAddress = (size_t)this->tableAddress;
    for (size_t pos = 0; pos < this->tableEntries; pos++)
    {
        Segment *storedSegment = (Segment *)tableEntryAddress;
        if ((size_t)address >= storedSegment->start && (size_t)address < storedSegment->end)
        {
            readOnly = storedSegment->flags & SEGF_RO;
            break;
        }
        tableEntryAddress += sizeof(Segment);
    }
    return readOnly;
}
