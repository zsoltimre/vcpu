/**
 * Segment Table
 *
 * Manage information about the different memory segments, such as the rstack
 * and the stack. The information is used to check for access violations, such
 * as attempts of out of bounds read/write.
 */

#ifndef __SEGMENT_TABLE_HEADER__
#define __SEGMENT_TABLE_HEADER__

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SEGMENT_NAME_MAX_LENGTH 8

#define SEGF_RW 0
#define SEGF_RO 1
#define SEGF_STATIC 2

struct Segment
{
    size_t start;
    size_t end;
    uint8_t flags;
    char name[SEGMENT_NAME_MAX_LENGTH];
};

class SegmentTable
{

    void *tableAddress = NULL;

public:
    size_t tableEntries = 0;
    SegmentTable(void *memory);
    void registerSegment(char *name, size_t start, size_t size, uint8_t flags);
    void getSegmentEntry(char *name, void *segment);
    bool isOutOfBounds(char *name, size_t address);
    bool isReadOnlyBySegmentName(char *name);
    bool isReadOnlyByAddress(void *address);
};

#endif
