#include "returntable.hpp"

ReturnTable::ReturnTable(SegmentTable *segmentTable, void *memory)
{
    this->tableAddress = memory;
    this->tableEntries = 0;
    this->segmentTable = segmentTable;
};

bool ReturnTable::addEntry(void *address, void *stack)
{
    size_t storeAt = (size_t)this->tableAddress;
    if (this->tableEntries > 0)
    {
        storeAt += this->tableEntries * sizeof(ReturnEntry);
    }

    if (this->segmentTable->isOutOfBounds((char *)"rstack", storeAt)) {
        return false;
    }

    memset((void *)storeAt, 0, sizeof(ReturnEntry));

    ReturnEntry returnEntry;
    returnEntry.address = address;
    returnEntry.stack = stack;

    memcpy((void *)storeAt, &returnEntry, sizeof(ReturnEntry));

    this->tableEntries++;
    return true;
}

bool ReturnTable::removeEntry()
{
    if (this->tableEntries == 0) {
        return false;
    }
    memset((void *)((size_t)this->tableAddress + sizeof(ReturnEntry) * (this->tableEntries - 1)), 0, sizeof(ReturnEntry));
    this->tableEntries--;
    return true;
}

ReturnEntry *ReturnTable::getLast()
{
    return (ReturnEntry *)((size_t)this->tableAddress + sizeof(ReturnEntry) * (this->tableEntries - 1));
}
