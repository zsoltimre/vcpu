#ifndef __RETURN_TABLE_HEADER__
#define __RETURN_TABLE_HEADER__

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "segmenttable.hpp"

struct ReturnEntry
{
    void *address;
    void *stack;
};

class ReturnTable
{

    void *tableAddress = NULL;
    SegmentTable *segmentTable = NULL;

public:
    size_t tableEntries = 0;
    ReturnTable(SegmentTable *segmentTable, void *memory);
    bool addEntry(void *address, void *stack);
    bool removeEntry();
    ReturnEntry *getLast();

};

#endif
