#ifndef __UTILS_HEADER__
#define __UTILS_HEADER__

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>

std::vector<std::string> split(const std::string &text, char sep);
bool shouldTrim(char c);
void trim(std::string &s);
std::string replaceAll(std::string target, std::string from, std::string to);
std::string stripComments(std::string line);
size_t stringValueToNumber(std::string value);
bool isSegment(struct State *state, std::string value);
void checkSegmentSize(size_t size);
size_t getValue(std::map<std::string, size_t> *source, std::string value);

#endif
