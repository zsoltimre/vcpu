#ifndef __SEGMENT_HEADER__
#define __SEGMENT_HEADER__

#include "state.hpp"
#include "utils.hpp"
#include "../common/file.hpp"

#define SEGMENT_NAME_RSTACK ".rstack"
#define SEGMENT_NAME_STACK  ".stack"
#define SEGMENT_NAME_CODE   ".code"

typedef void (*SegmentHandler)(
    struct State *state,
    Executable *executable,
    std::string value
);

bool isSegment(struct State *state, std::string value);
SegmentHandler getSegmentHandler(std::string segmentName);
void processSegment(struct State *state, Executable *executable, std::string segment, std::string value);

void handleSegmentRstack(struct State *state, Executable *executable, std::string value);
void handleSegmentStack(struct State *state, Executable *executable, std::string value);
void handleSegmentCode(struct State *state, Executable *executable, std::string value);

#endif
