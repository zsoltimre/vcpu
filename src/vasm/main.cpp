#include "main.hpp"

std::map<std::string, std::vector<size_t>> LabelLocations{};
std::map<std::string, size_t> LabelAddress{};
std::vector<size_t> compiledCode{};

int main(int argc, char *argv[])
{
    struct State state;
    struct Executable executable;

    state.codeSegment = false;
    // Segment size default values
    executable.rstackSize = 256;
    executable.stackSize = 256;

    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s <source> <outfile>\n", argv[0]);
        exit(1);
    }

    FILE *fd = fopen(argv[1], "r");
    if (fd == NULL) {
        fprintf(stderr, "Failed to open file: %s.\n", argv[1]);
        exit(1);
    }

    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, fd)) != -1) {
        processSourceLine(&LabelAddress, &LabelLocations, &state, &compiledCode, &executable, line);
    }

    fclose(fd);

    updateCodeLabelAddresses(&LabelAddress, &LabelLocations, &compiledCode);

    size_t size = 0;
    void *code = malloc(compiledCode.size() * sizeof(size_t));
    for (size_t pos = 0; pos < compiledCode.size(); pos++) {
        *(size_t *)((size_t)code + pos * sizeof(size_t)) = compiledCode.at(pos);
        size += sizeof(size_t);
    }

    serialize(argv[2], executable.rstackSize, executable.stackSize, code, size);

    free(code);
}
