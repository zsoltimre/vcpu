#include <gtest/gtest.h>

#include "segment.hpp"

TEST(isSegment, CheckIfSegment) {
    State state;
    state.codeSegment = true;
    bool result = isSegment(&state, ".stack");
    EXPECT_EQ(result, false);

    state.codeSegment = false;
    result = isSegment(&state, ".stack");
    EXPECT_EQ(result, true);
}

TEST(handleSegmentRstack, SetRstackSegmentSize) {
    State state;
    Executable executable;
    handleSegmentRstack(&state, &executable, "4096");
    EXPECT_EQ(executable.rstackSize, 4096);

    handleSegmentRstack(&state, &executable, "0x1000");
    EXPECT_EQ(executable.rstackSize, 4096);
}

TEST(handleSegmentStack, SetStackSegmentSize) {
    State state;
    Executable executable;
    handleSegmentStack(&state, &executable, "4096");
    EXPECT_EQ(executable.stackSize, 4096);

    handleSegmentStack(&state, &executable, "0x1000");
    EXPECT_EQ(executable.stackSize, 4096);
}

TEST(handleSegmentCode, SetCodeSegmentFlag) {
    State state;
    Executable executable;
    EXPECT_EQ(state.codeSegment, false);
    handleSegmentCode(&state, &executable, "");
    EXPECT_EQ(state.codeSegment, true);
}

TEST(getSegmentHandler, GetSegmentHandler) {
    SegmentHandler handler = getSegmentHandler(".unknown");
    bool found = handler != NULL;
    EXPECT_EQ(found, false);

    handler = getSegmentHandler(".rstack");
    found = handler != NULL;
    EXPECT_EQ(found, true);
}

TEST(processSegment, ProcessSegments) {
    State state;
    Executable executable;

    executable.rstackSize = 256;
    executable.stackSize = 256;

    processSegment(&state, &executable, ".rstack", "0x1000");
    EXPECT_EQ(executable.rstackSize, 4096);

    processSegment(&state, &executable, ".stack", "0xff");
    EXPECT_EQ(executable.stackSize, 255);
    EXPECT_EQ(executable.rstackSize, 4096);
}

TEST(processSegment, HandleUnknownSegment) {
    State state;
    Executable executable;

    executable.rstackSize = 256;
    executable.stackSize = 256;

    EXPECT_ANY_THROW({
        processSegment(&state, &executable, ".unsupported", "0x1000");
    });
}

TEST(processSegment, IgnoreSegmentDefinitionInCode) {
    State state;
    Executable executable;

    executable.rstackSize = 256;
    executable.stackSize = 256;

    state.codeSegment = true;
    processSegment(&state, &executable, ".rstack", "0x1000");
    EXPECT_EQ(executable.rstackSize, 256);
}
