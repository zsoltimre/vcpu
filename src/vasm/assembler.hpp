#ifndef __ASSEMBLER_HEADER__
#define __ASSEMBLER_HEADER__

#include <string>
#include <map>
#include <sstream>

#include "../vcpu/registers/registers.hpp"
#include "../vcpu/instructions/instructions.hpp"

#include "segment.hpp"
#include "utils.hpp"

enum ArgumentType : int
{
    TYPE_VALUE = 1,
    TYPE_REGISTER,
    TYPE_LABEL,
};

int getArgumentType(std::string argument);
bool isLabel(std::string value);
size_t getValueAsNumber(std::string value);
void registerLabelReferenceLocation(std::map<std::string, std::vector<size_t>> *labelLocations, std::string name, size_t location);
void updateCodeLabelAddresses(std::map<std::string, size_t> *labelAddress, std::map<std::string, std::vector<size_t>> *locations, std::vector<size_t> *code);
void processArgument(std::map<std::string, std::vector<size_t>> *labelLocations, std::vector<size_t> *code, std::string argument);
void processArguments(std::map<std::string, std::vector<size_t>> *labelLocations, std::vector<size_t> *code, std::vector<std::string> *op);
void removeEmptyElements(std::vector<std::string> *elements);
void processSourceLine(std::map<std::string, size_t> *labelAddress, std::map<std::string, std::vector<size_t>> *labelLocations, struct State *state, std::vector<size_t> *code, struct Executable *executable, std::string line);

#endif
