#include <gtest/gtest.h>

#include "assembler.hpp"

TEST(getArgumentType, GetArgumentType)
{
    EXPECT_EQ(getArgumentType("$111"), TYPE_VALUE);
    EXPECT_EQ(getArgumentType("%R01"), TYPE_REGISTER);
    EXPECT_EQ(getArgumentType("test:"), TYPE_LABEL);
}

TEST(isLabel, CheckIfLabel)
{
    EXPECT_EQ(isLabel("test:"), true);
    EXPECT_EQ(isLabel("test"), false);
}

TEST(getValueAsNumber, GetNumberFromString)
{
    EXPECT_EQ(getValueAsNumber("$111"), 111);
    EXPECT_EQ(getValueAsNumber("$0xff"), 255);
    EXPECT_EQ(getValueAsNumber("aaa"), 0);
}

TEST(registerLabelReferenceLocation, RegisterLocationLabelReferredFrom)
{
    std::map<std::string, std::vector<size_t>> labelLocations;

    registerLabelReferenceLocation(&labelLocations, "unittest_1", 128);
    registerLabelReferenceLocation(&labelLocations, "unittest_1", 255);
    registerLabelReferenceLocation(&labelLocations, "unittest_2", 65535);

    auto result_1 = labelLocations.find("unittest_1");
    EXPECT_NE(result_1, labelLocations.end());
    EXPECT_EQ(result_1->second[0], 128);
    EXPECT_EQ(result_1->second[1], 255);

    auto result_2 = labelLocations.find("unittest_2");
    EXPECT_NE(result_2, labelLocations.end());
    EXPECT_EQ(result_2->second[0], 65535);
}

TEST(updateCodeLabelAddresses, UpdateLabelAddressInCode)
{
    std::map<std::string, size_t> labelAddress{
        {"unittest", 255}};
    std::map<std::string, std::vector<size_t>> labelLocations;
    std::vector<size_t> code = {0, 0, 0, 0, 0, 0, 0, 0};

    registerLabelReferenceLocation(&labelLocations, "unittest", 1);
    registerLabelReferenceLocation(&labelLocations, "unittest", 7);

    updateCodeLabelAddresses(&labelAddress, &labelLocations, &code);
    EXPECT_EQ(code[1], 255);
    EXPECT_EQ(code[7], 255);
}

TEST(processArgument, ProcessArgument)
{
    std::map<std::string, std::vector<size_t>> labelLocations;
    std::vector<size_t> code = {};

    processArgument(&labelLocations, &code, "$255");
    EXPECT_EQ(code[0], 255);
    processArgument(&labelLocations, &code, "%R0");
    EXPECT_EQ(code[1], R0);
    processArgument(&labelLocations, &code, "test_label");
    EXPECT_EQ(code[2], 0);
    EXPECT_EQ(labelLocations["test_label"][0], 2);
}

TEST(processArgument, AddsLabelPlaceholder)
{
    std::map<std::string, std::vector<size_t>> labelLocations;
    std::vector<size_t> code = {CALL};

    EXPECT_EQ(code.size(), 1);
    processArgument(&labelLocations, &code, "test_label");
    EXPECT_EQ(code.size(), 2);
    EXPECT_EQ(code[1], 0);
    std::vector<size_t> locations = labelLocations["test_label"];
    EXPECT_EQ(locations[0], 1);
}

TEST(processArguments, ProcessArguments)
{
    std::map<std::string, size_t> labelAddress{};
    std::map<std::string, std::vector<size_t>> labelLocations;
    std::vector<size_t> code = {};

    std::vector<std::string> op = {"MOVR", "%R0", "$0xff"};
    processArguments(&labelLocations, &code, &op);
    EXPECT_EQ(code[0], R0);
    EXPECT_EQ(code[1], 255);
}

TEST(removeEmptyElements, RemoveEmptyElements)
{
    std::vector<std::string> elements = {"aaa", "", "bbb", "", "ccc"};
    removeEmptyElements(&elements);
    EXPECT_EQ(elements.size(), 3);
    EXPECT_EQ(elements[0], "aaa");
    EXPECT_EQ(elements[1], "bbb");
    EXPECT_EQ(elements[2], "ccc");
}

TEST(processSourceLine, ProcessSourceLine)
{
    std::map<std::string, size_t> labelAddress{};
    std::map<std::string, std::vector<size_t>> labelLocations;
    std::vector<size_t> code = {};
    State state;
    Executable executable;

    executable.rstackSize = 256;
    executable.stackSize = 256;

    state.codeSegment = true;
    processSourceLine(&labelAddress, &labelLocations, &state, &code, &executable, "MOVR %R0, $0xff");
    EXPECT_EQ(code[0], MOVR);
    EXPECT_EQ(code[1], R0);
    EXPECT_EQ(code[2], 255);
}
