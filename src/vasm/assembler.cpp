#include "assembler.hpp"

std::map<std::string, size_t> RegisterNameToId{
    {"IP", IP},
    {"SP", SP},
    {"BP", BP},
    {"RP", RP},
    {"FR", FR},
    {"EH", EH},
    {"FLR", FLR},
    {"R0", R0},
    {"R1", R1},
    {"R2", R2},
    {"R3", R3},
    {"R4", R4},
    {"R5", R5},
    {"R6", R6},
    {"R7", R7},
    {"R8", R8},
    {"R9", R9},
    {"R10", R10},
    {"R11", R11},
    {"R12", R12},
    {"R13", R13},
    {"R14", R14},
    {"R15", R15},
};

std::map<std::string, size_t> InstructionIdMap{
    {"HALT", HALT},
    {"SEH", SEH},
    {"ADD", ADD},
    {"ADDR", ADDR},
    {"SUB", SUB},
    {"SUBR", SUBR},
    {"MUL", MUL},
    {"MULR", MULR},
    {"DIV", DIV},
    {"DIVR", DIVR},
    {"CMP", CMP},
    {"CMPR", CMPR},
    {"PUSH", PUSH},
    {"PUSHR", PUSHR},
    {"PRINT", PRINT},
    {"POPR", POPR},
    {"STR", STR},
    {"LDR", LDR},
    {"MOV", MOV},
    {"MOVR", MOVR},
    {"JMP", JMP},
    {"JLT", JLT},
    {"JGT", JGT},
    {"JEQ", JEQ},
    {"JF", JF},
    {"CALL", CALL},
    {"PUTC", PUTC},
    {"RET", RET},
};

int getArgumentType(std::string argument)
{
    if (argument[0] == '$') return TYPE_VALUE;
    if (argument[0] == '%') return TYPE_REGISTER;
    return TYPE_LABEL;
}

bool isLabel(std::string value)
{
    return value[value.length() - 1] == ':';
}

size_t getValueAsNumber(std::string value)
{
    value = value.substr(1, value.length());
    return stringValueToNumber(value);
}

void registerLabelReferenceLocation(std::map<std::string, std::vector<size_t>> *labelLocations, std::string name, size_t location)
{
    labelLocations->insert(std::pair<std::string, std::vector<size_t>>(name, {}));
    auto result = labelLocations->find(name);
    result->second.push_back(location);
}

void updateCodeLabelAddresses(std::map<std::string, size_t> *labelAddress, std::map<std::string, std::vector<size_t>> *locations, std::vector<size_t> *code)
{
    for (std::map<std::string, std::vector<size_t>>::iterator iter = locations->begin(); iter != locations->end(); ++iter)
    {
        std::vector<size_t> positions = iter->second;
        for (size_t i = 0; i < positions.size(); i++)
        {
            code->at(positions[i]) = labelAddress->at(iter->first);
        }
    }
}

void processArgument(std::map<std::string, std::vector<size_t>> *labelLocations, std::vector<size_t> *code, std::string argument)
{
    int argType = getArgumentType(argument);
    if (argType == TYPE_LABEL) {
        registerLabelReferenceLocation(labelLocations, argument, code->size());
        code->push_back(0); // Add label placeholder
        return;
    }
    size_t value = argType == TYPE_REGISTER ?
        getValue(&RegisterNameToId, argument.substr(1, argument.length())) :
        getValueAsNumber(argument);
    code->push_back(value);
}

void processArguments(std::map<std::string, std::vector<size_t>> *labelLocations, std::vector<size_t> *code, std::vector<std::string> *op)
{
    for (size_t i = 1; i < op->size(); i++)
    {
        processArgument(labelLocations, code, op->at(i));
    }
}

void removeEmptyElements(std::vector<std::string> *elements) {
    for (size_t i = 0; i < elements->size(); i++)
    {
        if (elements->at(i).size() > 0)
            continue;
        elements->erase(elements->begin() + i);
        i--;
    }
}

void processSourceLine(std::map<std::string, size_t> *labelAddress, std::map<std::string, std::vector<size_t>> *labelLocations, struct State *state, std::vector<size_t> *code, struct Executable *executable, std::string line)
{
    line = stripComments(line);
    if (line == "")
        return;
    std::istringstream iss(line);

    line = replaceAll(line, ", ", ",");
    line = replaceAll(line, ",", " ");

    std::vector<std::string> op = split(line, 0x20);
    removeEmptyElements(&op);

    processSegment(state, executable, op[0], op[1]);
    if (!state->codeSegment || op[0].substr(0, 1) == ".")
        return;

    if (isLabel(op[0]))
    { // If label, store it's address
        std::string labelName = op[0].substr(0, op[0].length() - 1);
        labelAddress->insert({labelName, code->size()});
        return;
    }

    size_t instructionCode = getValue(&InstructionIdMap, op[0]);
    code->push_back(instructionCode);
    processArguments(labelLocations, code, &op);
}
