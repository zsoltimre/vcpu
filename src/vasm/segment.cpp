#include "segment.hpp"

/**
 * Check if the given source line is related to a segment by checking if the
 * first character is a dot (`.`).
 */
bool isSegment(struct State *state, std::string value)
{
    return (!state->codeSegment && value[0] == '.') ? true : false;
}

void handleSegmentRstack(struct State *state, Executable *executable, std::string value) {
    executable->rstackSize = stringValueToNumber(value);
}

void handleSegmentStack(struct State *state, Executable *executable, std::string value) {
    executable->stackSize = stringValueToNumber(value);
}

void handleSegmentCode(struct State *state, Executable *executable, std::string value) {
    state->codeSegment = true;
}

std::map<std::string, SegmentHandler> SegmentHandlerMap{
    { SEGMENT_NAME_RSTACK, handleSegmentRstack },
    { SEGMENT_NAME_STACK, handleSegmentStack },
    { SEGMENT_NAME_CODE, handleSegmentCode },
};

SegmentHandler getSegmentHandler(std::string segmentName)
{
    const auto handler = SegmentHandlerMap.find(segmentName);
    if (handler == SegmentHandlerMap.end())
    {
        return NULL;
    }
    return handler->second;
}

void processSegment(struct State *state, Executable *executable, std::string segment, std::string value)
{
    if (!isSegment(state, segment))
        return;
    SegmentHandler handler = getSegmentHandler(segment);
    if (handler == NULL) {
        throw std::invalid_argument("Invalid segment type.");
    }
    handler(state, executable, value);
}
