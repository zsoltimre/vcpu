#include <gtest/gtest.h>

#include "utils.hpp"

TEST(split, SplitString) {
    std::string testString = "first:second";
    std::vector<std::string> result = split(testString, ':');
    EXPECT_EQ(result.size(), 2);
    EXPECT_EQ(result[0], "first");
    EXPECT_EQ(result[1], "second");
}

TEST(shouldTrim, ShouldTrimString) {
    EXPECT_EQ(shouldTrim('x'), false);
    EXPECT_EQ(shouldTrim(' '), true);
    EXPECT_EQ(shouldTrim('\t'), true);
    EXPECT_EQ(shouldTrim('\n'), true);
}

TEST(trim, TrimString) {
    std::string testString1 = " hi there  ";
    trim(testString1);
    EXPECT_EQ(testString1, "hi there");

    std::string testString2 = "first\nsecond\n\n";
    trim(testString2);
    EXPECT_EQ(testString2, "first\nsecond");
}

TEST(replaceAll, ReplaceAllOccurrences) {
    std::string testString = "This:here:is:test";
    testString = replaceAll(testString, ":", " ");
    EXPECT_EQ(testString, "This here is test");
}

TEST(stripComments, StripFullLineComment) {
    std::string testString = "# This:here:is:test";
    testString = stripComments(testString);
    EXPECT_EQ(testString.size(), 0);

    testString = "  # This:here:is:test";
    testString = stripComments(testString);
    EXPECT_EQ(testString.size(), 0);
}

TEST(stripComments, StringComment) {
    std::string testString = "test # This is comment";
    testString = stripComments(testString);
    EXPECT_EQ(testString, "test");
}

TEST(stringValueToNumber, StringToNumber) {
    EXPECT_EQ(stringValueToNumber("12"), 12);
    EXPECT_EQ(stringValueToNumber("0xff"), 255);
}

TEST(getValue, GetValueFromMap) {
    std::map<std::string, size_t> source{
        { "TEST_1", 1 },
        { "TEST_2", 2 },
    };
    size_t result = getValue(&source, "test_1");
    EXPECT_EQ(result, 1);
    result = getValue(&source, "test_2");
    EXPECT_EQ(result, 2);
    result = getValue(&source, "unknown");
    EXPECT_EQ(result, 0);
}
