#ifndef __MAIN_HEADER__
#define __MAIN_HEADER__

#include <string>
#include <vector>
#include <map>

#include <stdio.h>

#include "../common/file.hpp"
#include "../libserializer/serializer.hpp"

#include "state.hpp"
#include "assembler.hpp"

#endif
