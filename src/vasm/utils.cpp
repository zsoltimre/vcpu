#include "utils.hpp"
#include "state.hpp"

// https://stackoverflow.com/a/7408245
std::vector<std::string> split(const std::string &text, char sep)
{
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos)
    {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

bool shouldTrim(char c) {
    return std::isspace(c) || c == '\t';
}

// https://stackoverflow.com/a/33129986
void trim(std::string &s)
{
    s.erase(s.begin(), std::find_if_not(
        s.begin(),
        s.end(),
        [](char c) { return shouldTrim(c); })
    );
    s.erase(
        std::find_if_not(s.rbegin(),
        s.rend(),
        [](char c) { return shouldTrim(c); }).base(),
        s.end()
    );
}

std::string replaceAll(std::string target, std::string from, std::string to) {
    size_t pos = target.find(from);
    while (pos != std::string::npos) {
        target = target.replace(pos, from.size(), to);
        pos = target.find(from);
    }
    return target;
}

std::string stripComments(std::string line)
{
    trim(line);
    if (line.substr(0, 1) == "#")
    {
        return "";
    }
    line = split(line, '#')[0];
    trim(line);
    return line;
}

size_t stringValueToNumber(std::string value)
{
    const char *v = value.c_str();
    return strtoul(v, (char **)&v[strlen(v)], 0);
}

size_t getValue(std::map<std::string, size_t> *source, std::string value)
{
    std::transform(value.begin(), value.end(), value.begin(), ::toupper);
    const auto result = source->find(value);
    if (result == source->end())
    {
        return 0;
    }
    return result->second;
}
