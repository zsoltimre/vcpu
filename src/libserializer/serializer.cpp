#include "serializer.hpp"

extern "C" void serialize(char *fileName, size_t rstackSize, size_t stackSize, void *code, size_t size)
{
    FILE *fd = fopen(fileName, "wb");
    if (fd == NULL)
    {
        fprintf(stderr, "Failed to create executable file.\n");
        exit(1);
    }
    fwrite((char *)"vCPU", 4, 1, fd);
    fwrite(&rstackSize, sizeof(size_t), 1, fd);
    fwrite(&stackSize, sizeof(size_t), 1, fd);
    fwrite(code, size, 1, fd);
    fclose(fd);
}

extern "C" size_t deserialize(char *fileName, ExecOptions *execOpts)
{
    FILE *fd = fopen(fileName, "rb");
    if (fd == NULL)
    {
        fprintf(stderr, "Failed to open executable file.\n");
        exit(1);
    }

    struct stat fst;
    stat(fileName, &fst);
    char fileData[fst.st_size];
    fread(fileData, fst.st_size, 1, fd);

    memcpy(&execOpts->magic, fileData, 4);
    execOpts->rstackSize = *(size_t *)(fileData + 4);
    execOpts->stackSize = *(size_t *)(fileData + sizeof(size_t) + 4);

    size_t codeSize = fst.st_size - (sizeof(size_t) * 2) - 4;
    size_t codeStart = fst.st_size - codeSize;

    for (size_t pos = 0; pos < codeSize; pos += sizeof(size_t))
    {
        size_t sourceAddress = (size_t)fileData + codeStart + pos;
        size_t targetAddress = (size_t)execOpts->memory + pos;
        *(size_t *)targetAddress = *(size_t *)sourceAddress;
    }
    return codeSize;
}
