#include <gtest/gtest.h>

#include "serializer.hpp"

uint8_t FILE_CONTENT[] = {
    // Magic value: "vCPU"
    0x76, 0x43, 0x50, 0x55,
    // File header (segment sizes)
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    // Code segment content
    0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

TEST(serialize, SerializeIntoFile) {
    char fileName[] = "unittest_serialize.bin";

    uint8_t code[24] = {
        0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    serialize(fileName, 0x01, 0x02, &code, 24);

    struct stat fst;
    stat(fileName, &fst);
    char fileData[fst.st_size];
    FILE *fd = fopen(fileName, "rb");
    fread(fileData, fst.st_size, 1, fd);
    fclose(fd);

    for (unsigned int pos = 0; pos < 44; pos++) {
        EXPECT_EQ((uint8_t)fileData[pos], FILE_CONTENT[pos]);
    }
}

TEST(deserialize, DeserializeFromFile) {
    ExecOptions execOpts;
    execOpts.memory = malloc(128);

    char fileName[] = "unittest_deserialize.bin";

    FILE *fd = fopen(fileName, "wb");
    fwrite(FILE_CONTENT, sizeof(FILE_CONTENT), 1, fd);
    fclose(fd);

    size_t codeSize = deserialize(fileName, &execOpts);
    EXPECT_GE(codeSize, 24);
    EXPECT_EQ(execOpts.rstackSize, 1);
    EXPECT_EQ(execOpts.stackSize, 2);

    EXPECT_EQ(*(size_t *)((size_t)execOpts.memory + sizeof(size_t) * 0), 0xff);
    EXPECT_EQ(*(size_t *)((size_t)execOpts.memory + sizeof(size_t) * 1), 0xaa);
    EXPECT_EQ(*(size_t *)((size_t)execOpts.memory + sizeof(size_t) * 2), 0x04);

    if (execOpts.memory != NULL) {
        free(execOpts.memory);
    }
}
