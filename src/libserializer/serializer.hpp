#ifndef __SERIALIZER_HEADER__
#define __SERIALIZER_HEADER__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "../vcpu/loader/loader.hpp"

extern "C" void serialize(char *fileName, size_t rstackSize, size_t stackSize, void *code, size_t size);
extern "C" size_t deserialize(char *fileName, struct ExecOptions *execOpts);

#endif
