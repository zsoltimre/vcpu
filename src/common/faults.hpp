#ifndef __FILE_HEADER__
#define __FILE_HEADER__

#define FAULT_DIVZERO               0x01    // Division by zero
#define FAULT_STACK_OOBOP           0x02    // Stack out of bounds operation
#define FAULT_STACK_CORRUPTION      0x03    // Stack corruption
#define FAULT_RSTACK_OOBOP          0x04    // Rstack out of bounds operation
#define FAULT_SEG_FORBIDDEN_WRITE   0x05    // Attempt to write to read-only segment

#endif
