#ifndef __FILE_HEADER__
#define __FILE_HEADER__

#include <stdlib.h>

#define FILE_MAGIC  "vCPU"

struct Executable {
    char magic[4];
    size_t rstackSize;
    size_t stackSize;
};

#endif
