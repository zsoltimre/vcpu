# =============================================================================
# This program demonstrates what happens if we attempt to write to a segment
# (memory area) that was marked as read-only. Such a memory address is, for 
# example, the return address stack (R-stack).
# =============================================================================

.stack 0x100         # Stack segment size
.rstack 0x100        # Return Address Stack segment size
.code
    seh handle_error
    call write_to_protected_segment
    mov %R0, $0
    halt %R0
handle_error:
    halt %FR
write_to_protected_segment:
    # Attempt to write somewhere inside the R-stack memory area. Please note
    # the return code (`echo $?`) once the executable terminates.
    
    movr %R0, %RP
    add %R0, $8

    # Change the above instructions to write to the standard stack and see
    # how the return code changes.

    mov %R1, $0xffffffffffffffff

    # Write to the memory area.
    str %R1, %R0
    ret
