# =============================================================================
# This program demonstrates how a system that implements the "Return Address 
# Protection" experiment looks like. (see: ../experiments/safe-retaddr.md)
# =============================================================================

.stack 16                       # Stack size (in bytes)
.rstack 16                      # Return address stack size (in bytes)
.code
    seh exit_with_error
    call function
exit_normally:
    push $0x00000000000a4b4f    # Place string ("OK\n") on the stack
    movr %R0, %SP               # Move string address into R0
    mov %R1, $3                 # 3 characters remaining
    call print_string
    mov %R0, $0
    halt %R0
exit_with_error:
    halt %FR
# -----------------------------------------------------------------------------
# Here we simulate a function that due to a bug corrupts the values on the 
# stack.
# -----------------------------------------------------------------------------
function:
    # [0] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # ???       | /////////////////////// |  < SP
    #           |-------------------------|

    pushr %BP

    # [1] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | BP                      |  < SP
    #           |-------------------------|
    # ???       | /////////////////////// |
    #           |-------------------------|

    movr %BP, %SP

    # [2] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | BP                      |  < SP, BP
    #           |-------------------------|
    # ???       | /////////////////////// |
    #           |-------------------------|

    mov %R2, $0xffffffffffffffff    # Value to overwrite with
    str %R2, %SP                    # Corrupt value on stack

    # There's nothing else to corrupt on the stack. The return address is
    # not there.
    # Also, it would not make sense to try to write below S+0 as that area
    # is not part of the stack, thus the out-of-bounds checks would catch
    # the attempt.

    # [3] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | ff ff ff ff ff ff ff ff |  < SP, BP
    #           |-------------------------|
    # ???       | /////////////////////// |
    #           |-------------------------|

    movr %SP, %BP

    # [4] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | ff ff ff ff ff ff ff ff |  < BP, SP
    #           |-------------------------|
    # ???       | /////////////////////// |
    #           |-------------------------|

    popr %BP

    # [5] Stack snapshot:
    #
    #           |-------------------------|
    # S+8       | 00 00 00 00 00 00 00 00 |
    #           |-------------------------|
    # S+0       | ff ff ff ff ff ff ff ff |  < BP
    #           |-------------------------|
    # ???       | /////////////////////// |  < SP
    #           |-------------------------|

    ret
# -----------------------------------------------------------------------------
# Print string function
#
# Arguments:
#
#  R0:  Address of string to print
#  R1:  Length of string
#
# -----------------------------------------------------------------------------
print_string:
    putc %R0                    # Print character at addr in R1
    add %R0, $1                 # Move to next byte
    sub %R1, $1                 # Reduce remaining characters
    cmp %R1, $0                 # Do we have any more characters left?
    jgt print_string            # If yes, jump to beginning of loop
    ret                         # if not, return to caller
