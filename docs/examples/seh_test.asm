.stack 16                       # Stack size (in bytes)
.rstack 16                      # Return address stack size (in bytes)
.code
    push $0
    seh exit_with_error
    mov %R0, $0
    halt %R0
exit_with_error:
    halt %FR
