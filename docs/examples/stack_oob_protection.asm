# =============================================================================
# This program demonstrates how the stack out of bounds access protection 
# works.
# 
# Attempts to access memory outside of the stack segment causes an error. The
# error is signalled by setting the value of the `FR` register to greater
# than `0`.
# =============================================================================

.stack 0x100         # Stack segment size
.rstack 0x100        # Return Address Stack segment size
.code
    seh handle_error
    mov %R0, $0xffffffffffffffff
overflow_stack:
    pushr %R0
    # Uncomment the below instruction to see how the return value changes
    # in case of an OOB (over-read) stack operation.
    #
    # jmp overflow_stack
    #
underflow_stack:
    popr %R0
    # Uncomment the below instruction to see how the return value changes
    # in case of an OOB (under-read) stack operation.
    #
    # jmp underflow_stack
    #
exit_normally:
    mov %R0, $0
    halt %R0
handle_error:
    halt %FR
