# =============================================================================
# Simple program that prints out "Hello!" to the screen and terminates.
#
# At the time this example was created quite a few things were not implemented,
# so the code stores the string to be printed on the stack.
# =============================================================================

.stack 24                       # Stack size (in bytes)
.rstack 24                      # Return address stack size (in bytes)
.code
    seh handle_error            # Set global error handler
    push $0x000a216f6c6c6548    # Store string ("Hello!\n") on the stack
    movr %R0, %SP               # Move string address into R0
    mov %R1, $7                 # 7 characters remaining
    call print_string
exit_normally:
    mov %R0, $0                 # Set return code
    halt %R0                    # Exit with return code
handle_error:
    halt %FR                    # Exit with error code
# -----------------------------------------------------------------------------
# Print string function
#
# Arguments:
#
#  R0:  Address of string to print
#  R1:  Length of string
#
# -----------------------------------------------------------------------------
print_string:
    putc %R0                    # Print character at addr in R1
    add %R0, $1                 # Move to next byte
    sub %R1, $1                 # Reduce remaining characters
    cmp %R1, $0                 # Do we have any more characters left?
    jgt print_string            # If yes, jump to beginning of loop
    ret                         # if not, return to caller
