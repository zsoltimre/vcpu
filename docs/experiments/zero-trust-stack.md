# Fringe Experiment: Zero Trust Stack

## Warning

This experiment ignores decades of advances in computer sciences, best practices and conventions to experiment freely with things that may seem nonsense, insane, dangerous, unconventional, weird, awkward, whatever. I do this to have fun, learn and maybe, who knows, come up with something new and better.

## Experiment

In the [previous experiment](./experiment-safe-retaddr.md) I decided to store the return addresses in a dedicated, stack-like are that user code cannot modify. Preventing potential corruption of the return address, while not preventing the corruption of the remaining values comes with problems. In this experiment I attempt to implement protection(s) for the stack. Similarly to the previous experiment, the goal is to implement a solution where the safety of the stack is **by design** rather than an afterthought.

## Rationale

The way I see it, there are two key issues with how the stack was traditionally implemented:

 1. One function can corrupt the stack area of another function. This is because the stack is a contiguous memory area shared by the entire executable.
 2. The corruption of the individual values on the stack are not detected.

In both cases stack canaries can mitigate the problem to an extent, but they do not solve the problem. For example:

 1. A stack canary can be used to detect if a buggy function corrupts another stack frame. However, the response to such corruption is the termination of the program. This is clearly something I want to avoid as you can see from the previous experiment.
 2. When using stack canaries the stack corruption is only detected if the canary gets corrupted. This means, as long as we do not tamper with the canary, we can mess up other values on the stack and let execution continue. The previous experiment gives a few basic examples on how such a scenario can impact confidentiality, integrity and availability.

**TBD**

## Implementation

TODO

## Dangers

TODO

## Conclusion

TODO

## Comments and Feedbacks

Constructive feedbacks are always welcome and highly appreciated. You can leave your feedback by opening a new issue.
