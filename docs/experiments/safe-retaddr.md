# Fringe Experiment: Return Address Protection

## Warning

This experiment ignores decades of advances in computer sciences, best practices and conventions to experiment freely with things that may seem nonsense, insane, dangerous, unconventional, weird, awkward, whatever. I do this to have fun, learn and maybe, who knows, come up with something new and better.

## Experiment

The idea is to prevent the corruption of return addresses in case of the corruption of the stack. As a result, we could always return from a function call to the right address, even if the stack got corrupted. The goal is to implement a solution where the safety of the return addresses are **by design** rather than an afterthought (e.g. stack canaries).

## Rationale

[Stack canaries](https://en.wikipedia.org/wiki/Stack_buffer_overflow#Stack_canaries) are used to prevent attackers to manipulate the execution flow by detecting the corruption of return addresses on the stack. The application is terminated if the return address was found corrupted.

While stack canaries make exploitaiton more difficult, they do not prevent corruption. The implementation proposed by this experiment prevents the corruption of the return addresses. A positive side-effect of the implementation is that stack canaries are no longer required.

When using stack canaries the execution terminates when stack corruption is detected. We must consider that the corruption of values other than the return address on the stack can also have serious consequences. Division by zero, NULL pointer dereference, bypassing security controls are just three examples.

The picture below illustrates how the stack looks like and where the stack canary is placed.

![Stack Corruption](../assets/images/stack_corruption_illustration.png)

As we can see, due to the location of the canary we only detect stack corruption if the canary itself is damaged. This means that an attacker could compromise all the values on the stack before the canary and execution would continue. The implementation proposed by this experiment does not detect stack corruption at all. As a result, corrupted values may be returned to the caller, which can have unforeseen consequences. However, the exact same applies when the stack canary and the return address are present on the stack, but the attacker decides not to tamper with the canary or the return address. Therefore, the proposed implementation still seems like a good idea.

## Implementation

 * Return addresses are stored on a dedicated, stack-like memory area (RSTACK). The CPU updates this memory area when performing CALL and RET instructions. RSTACK is read-only for user code. 
 * The dedicated "link register" (Return Pointer / RP) points to the top of RSTACK. The CPU updates the value of this register when performing CALL and RET instructions. The value of the register is read-only for user code.

This is very similar to what Microsoft refers to as [Shadow Stack](https://techcommunity.microsoft.com/t5/windows-os-platform-blog/understanding-hardware-enforced-stack-protection/ba-p/1247815). Some of the key differences are:

| Microsoft's Implementation | Proposed Implementation    |
| -------------------------- | -------------------------- |
| Uses the shadow stack as a baseline for comparion. On RET the return address on the stack is compared to the return address in the shadow stack. If there's a mismatch, the execution is terminated. | Uses the "shadow stack" (rstack) as the source of truth to prevent corruption in the first place. On RET the return address is loaded from the rstack. Since there's no way to corrupt the rstack, no need to terminate program execution at any point. This implementation does not duplicate return addresses. |
| Hardware register SSP holds the Shadow Stack Pointer address. | Hardware register RP (return pointer) points to the top of the rstack. |
| New instructions (specifically INCSSP and RDSSP) are added for management of shadow stack pages. | No new instructions are added. The CALL and RET instructions automatically manage rstack. |
| SAVEPREVSSP/RSTORSSP – save/restore shadow stack (i.e. thread switching) | Not implemented. |

## Conclusion

The proposed implementation prevents the corruption of return addresses and eliminates the need for stack canaries. The remaining values on the stack are still exposed to attackers, exactly the same way as they were exposed when the stack canary was present.

Future experiments will attempt to address additional stack-related security concerns by trying to eliminate issues by design and not via hacks/patches/workarounds.

## Comments and Feedbacks

Constructive feedbacks are always welcome and highly appreciated. You can leave your feedback by opening a new issue.
