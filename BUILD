cc_library(
    name = "serializer",
    srcs = [
        "src/vcpu/loader/loader.hpp",
        "src/vcpu/utils/utils.hpp",
        "src/libserializer/serializer.cpp",
    ],
    hdrs = [
        "src/libserializer/serializer.hpp",
    ],
)

cc_binary(
    name = "vasm",
    srcs = [
        "src/common/file.hpp",
        "src/vcpu/cpu.hpp",
        "src/vasm/state.hpp",
        "src/vasm/assembler.hpp",
        "src/vasm/assembler.cpp",
        "src/vasm/segment.hpp",
        "src/vasm/segment.cpp",
        "src/vcpu/registers/registers.hpp",
        "src/vcpu/instructions/instructions.hpp",
        "src/vcpu/memory/segmenttable.hpp",
        "src/vasm/utils.hpp",
        "src/vasm/utils.cpp",
        "src/vasm/main.hpp",
        "src/vasm/main.cpp",
    ],
    deps = [":serializer"]
)

cc_binary(
    name = "vcpu",
    srcs = [
        "src/common/faults.hpp",
        "src/vcpu/utils/utils.hpp",
        "src/vcpu/utils/utils.cpp",
        "src/vcpu/loader/loader.hpp",
        "src/vcpu/loader/loader.cpp",
        "src/vcpu/registers/registers.hpp",
        "src/vcpu/registers/registers.cpp",
        "src/vcpu/memory/segmenttable.hpp",
        "src/vcpu/memory/segmenttable.cpp",
        "src/vcpu/memory/returntable.hpp",
        "src/vcpu/memory/returntable.cpp",
        "src/vcpu/instructions/instructions.hpp",
        "src/vcpu/instructions/instructions.cpp",
        "src/vcpu/cpu.hpp",
        "src/vcpu/cpu.cpp",
        "src/vcpu/main.cpp",
    ],
    deps = [":serializer"]
)

cc_test(
    name = "serializer_test",
    size = "small",
    srcs = [
        "src/vcpu/loader/loader.hpp",
        "src/libserializer/serializer.hpp",
        "src/libserializer/serializer.cpp",
        "src/libserializer/serializer_test.cpp",
    ],
    deps = [
        ":serializer",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "segment_test",
    size = "small",
    srcs = [
        "src/common/file.hpp",    
        "src/vasm/state.hpp",
        "src/vasm/utils.hpp",
        "src/vasm/utils.cpp",
        "src/vasm/segment.hpp",
        "src/vasm/segment.cpp",
        "src/vasm/segment_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vasm_utils_test",
    size = "small",
    srcs = [
        "src/vasm/state.hpp",
        "src/vasm/utils.hpp",
        "src/vasm/utils.cpp",
        "src/vasm/utils_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "assembler_test",
    size = "small",
    srcs = [
        "src/vcpu/registers/registers.hpp",
        "src/vcpu/instructions/instructions.hpp",
        "src/vcpu/cpu.hpp",
        "src/vcpu/memory/segmenttable.hpp",
        "src/vcpu/loader/loader.hpp",
        "src/libserializer/serializer.hpp",
        "src/vcpu/utils/utils.hpp",
        "src/vasm/segment.hpp",
        "src/vasm/segment.cpp",
        "src/vasm/state.hpp",
        "src/vasm/utils.hpp",
        "src/vasm/utils.cpp",
        "src/common/file.hpp",
        "src/vasm/assembler.hpp",
        "src/vasm/assembler.cpp",
        "src/vasm/assembler_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vcpu_instructions_test",
    size = "small",
    srcs = [
        "src/common/faults.hpp",
        "src/vcpu/registers/registers.hpp",
        "src/vcpu/registers/registers.cpp",
        "src/vcpu/memory/segmenttable.hpp",
        "src/vcpu/memory/segmenttable.cpp",
        "src/vcpu/cpu.hpp",
        "src/vcpu/loader/loader.hpp",
        "src/libserializer/serializer.hpp",
        "src/vcpu/utils/utils.hpp",
        "src/vcpu/instructions/instructions.hpp",
        "src/vcpu/instructions/instructions.cpp",
        "src/vcpu/instructions/instructions_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vcpu_segmenttable_test",
    size = "small",
    srcs = [
        "src/vcpu/memory/segmenttable.hpp",
        "src/vcpu/memory/segmenttable.cpp",
        "src/vcpu/memory/segmenttable_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vcpu_returntable_test",
    size = "small",
    srcs = [
        "src/vcpu/memory/segmenttable.hpp",
        "src/vcpu/memory/segmenttable.cpp",
        "src/vcpu/memory/returntable.hpp",
        "src/vcpu/memory/returntable.cpp",
        "src/vcpu/memory/returntable_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vcpu_utils_test",
    size = "small",
    srcs = [
        "src/vcpu/utils/utils.hpp",
        "src/vcpu/utils/utils.cpp",
        "src/vcpu/utils/utils_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "vcpu_registers_test",
    size = "small",
    srcs = [
        "src/vcpu/registers/registers.hpp",
        "src/vcpu/registers/registers.cpp",
        "src/vcpu/registers/registers_test.cpp",
    ],
    deps = [
        "@com_google_googletest//:gtest_main",
    ],
)
